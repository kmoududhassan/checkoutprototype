﻿using System;
using Checkout.Domain.Entities;

namespace Checkout.Domain.Services
{
    public interface IBasketService
    {
        Result<BasketItem> AddItemToBasket(BasketItem basketItem);
        Result<BasketItem> UpdateBasketItem(BasketItem basketItem);
        Result<Guid> DeleteBasketItem(Guid basketItemGuid);
        Result<Basket> ClearBasket(Guid basketGuid);
        BasketItem GetBasketItem(Guid basketItemGuid);
        Basket GetBasket(Guid basketGuid);
        void ArchiveBasket(Guid basketGuid);
        bool IsBasketExist(Guid basketGuid);
        bool IsBasketItemExist(Guid basketItemGuid);
        bool IsProductExist(Guid productGuid);
        Basket CreateEmptyBasket();
    }
}
