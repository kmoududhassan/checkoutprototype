﻿namespace Checkout.Domain.Services
{    
    public class Result<TEntity>
    {
        public Result(bool success)
        {
            Success = success;
        }

        public bool Success { get; private set; }

        public string Message { get; set; }

        public TEntity Entity { get; set; }
    }
}