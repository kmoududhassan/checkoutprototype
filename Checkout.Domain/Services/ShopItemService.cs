﻿using System;
using System.Linq;
using Checkout.Domain.Entities;
using System.Collections.Generic;


namespace Checkout.Domain.Services
{
    public class ShopItemService : IShopItemService
    {
        private readonly IEntityRepository<Product> _productRepository;

        public ShopItemService(IEntityRepository<Product> productRepository)
        {
            _productRepository = productRepository;
        }
        public IList<Product> GetProducts()
        {
            return _productRepository.GetAll().ToList();
        }
    }
}
