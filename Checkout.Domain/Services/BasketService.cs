﻿using System;
using System.Net;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

using Checkout.Domain.Entities;

namespace Checkout.Domain.Services
{
    public class BasketService : IBasketService
    {
        private readonly IEntityRepository<BasketItem> _basketItemRepository;
        private readonly IEntityRepository<Basket> _basketRepository;
        private readonly IEntityRepository<Product> _productRepository;

        public BasketService(
            IEntityRepository<BasketItem> basketItemRepository, 
            IEntityRepository<Basket> basketRepository,
            IEntityRepository<Product> productRepository)
        {
            _basketItemRepository = basketItemRepository;
            _basketRepository = basketRepository;
            _productRepository = productRepository;
        }

        public BasketItem GetBasketItem(Guid basketItemGuid)
        {
            return _basketItemRepository.AllIncluding(x => x.Product)
                    .FirstOrDefault(x => x.Guid == basketItemGuid);
        }

        public Basket GetBasket(Guid guid)
        {
            return _basketRepository.GetAll()
                    .Include(x => x.BasketItems)
                    .ThenInclude(y => y.Product)
                    .FirstOrDefault(x => x.Guid == guid);
        }

        public Result<BasketItem> AddItemToBasket(BasketItem item)
        {
            if (item.BasketGuid == Guid.Empty)
            {
                var basket = CreateEmptyBasket();
                item.BasketGuid = basket.Guid;
            }

            item.Guid = Guid.NewGuid();
            _basketItemRepository.Add(item);

            bool success = _basketItemRepository.Save();

            return new Result<BasketItem>(success)
            {                
                Entity = GetBasketItem(item.Guid)
            };
        }

        public Result<Basket> ClearBasket(Guid guid)
        {
            var basket = GetBasket(guid);

            basket.BasketItems.Clear();

            bool success = _basketRepository.Save();

            return new Result<Basket>(success)
            {
                Entity = basket
            };
        }

        public Result<Guid> DeleteBasketItem(Guid basketItemguid)
        {
            var item = new BasketItem { Guid = basketItemguid };
            _basketItemRepository.Delete(item);

            bool success = _basketItemRepository.Save();

            return new Result<Guid>(success)
            {
                Entity = basketItemguid
            };
        }

        public Result<BasketItem> UpdateBasketItem(BasketItem item)
        {            
            var entity = GetBasketItem(item.Guid);
            entity.Quantity = item.Quantity;

            bool success = _basketItemRepository.Save();

            return new Result<BasketItem>(success)
            {
                Entity = entity
            };
        }

        public bool IsBasketExist(Guid basketGuid)
        {
            return _basketRepository.GetAll().Any(basketGuid);
        }

        public bool IsProductExist(Guid guid)
        {
            return _productRepository.GetAll().Any(guid);
        }

        public Basket CreateEmptyBasket()
        {
            var basket = new Basket
            {
                Guid = Guid.NewGuid(),
                ExpireOn = DateTime.Now.AddDays(7),
                IsActive = true
            };
            _basketRepository.Add(basket);
            _basketRepository.Save();
            return basket;
        }

        public bool IsBasketItemExist(Guid basketItemGuid)
        {
            return _basketItemRepository.GetAll().Any(basketItemGuid);
        }

        public void ArchiveBasket(Guid basketGuid)
        {
            var basket = new Basket
            {
                Guid = basketGuid,
                ExpireOn = DateTime.Now,
                IsActive = false,
                ModifiedDate = DateTime.Now
            };
            _basketRepository.Edit(basket);
            _basketRepository.Save();
        }
    }
}
