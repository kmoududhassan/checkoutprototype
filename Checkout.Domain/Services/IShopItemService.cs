﻿using System;
using Checkout.Domain.Entities;
using System.Collections.Generic;

namespace Checkout.Domain.Services
{
    public interface IShopItemService
    {
        IList<Product> GetProducts();        
    }
}
