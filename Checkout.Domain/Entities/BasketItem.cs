﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Checkout.Domain.Entities
{
    public class BasketItem : IEntity
    {
        [Key]
        public Guid Guid { get; set; }

        public Guid BasketGuid { get; set; }

        public Guid ProductGuid { get; set; }
        
        public int Quantity { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual Product Product { get; set; }                
    }
}