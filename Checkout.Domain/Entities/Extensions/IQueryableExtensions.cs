﻿using System;
using System.Linq;

namespace Checkout.Domain.Entities
{    
    public static class IQueryableExtensions
    {       
        public static bool Any(this IQueryable<IEntity> query, Guid guid)
        {
            return query.Any(x => x.Guid == guid);
        }
    }
}