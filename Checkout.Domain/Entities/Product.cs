﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Checkout.Domain.Entities
{
    public class Product : IEntity
    {
        [Key]
        public Guid Guid { get; set; }
                
        public decimal Price { get; set; }        

        public ProductStatus ProductStatus { get; set; }

        [Required]
        [StringLength(50)]
        public string ProductName { get; set; }
        
        public DateTime CreateDate { get; set; }
        public DateTime? ModifiedDate { get; set; }        
    }
}