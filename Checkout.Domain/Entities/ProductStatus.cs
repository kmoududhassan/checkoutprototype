﻿namespace Checkout.Domain.Entities
{
    public enum ProductStatus
    {
        Available,
        OutofStock,
        Discontinued
    }
}
