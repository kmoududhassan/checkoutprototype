﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Checkout.Domain.Entities
{
    public class Basket : IEntity
    {
        [Key]
        public Guid Guid { get; set; }

        public bool IsActive { get; set; }
        public DateTime ExpireOn { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? UserGuid { get; set; }

        public virtual ICollection<BasketItem> BasketItems { get; set; }

        public Basket()
        {
            BasketItems = new HashSet<BasketItem>();
        }
    }
}
