﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Checkout.Domain.Entities
{
    public interface IEntityRepository<T> where T : class, IEntity, new()
    {
        IQueryable<T> AllIncluding(params Expression<Func<T, object>>[] includeProperties);
        IQueryable<T> GetAll();
        T GetSingle(Guid guid);
        IQueryable<T> FindBy(Expression<Func<T, bool>> predicate);        

        void Add(T entity);
        void Edit(T entity);
        void Delete(T entity);        
        bool Save();
    }
}