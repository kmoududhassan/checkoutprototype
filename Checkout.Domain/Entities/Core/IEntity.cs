﻿using System;

namespace Checkout.Domain.Entities
{
    public interface IEntity
    {
        Guid Guid { get; set; }
        DateTime CreateDate { get; set; }
        DateTime? ModifiedDate { get; set; }
    }
}