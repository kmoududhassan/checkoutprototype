﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Checkout.Domain.Entities
{
    public class EntityRepository<T> : IEntityRepository<T>
        where T : class, IEntity, new()
    {
        readonly DbContext _entitiesContext;

        public EntityRepository(DbContext entitiesContext)
        {
            _entitiesContext = entitiesContext ?? throw new ArgumentNullException("entitiesContext");
        }

        public virtual IQueryable<T> GetAll()
        {
            return _entitiesContext.Set<T>();
        }

        public virtual IQueryable<T> AllIncluding(params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = _entitiesContext.Set<T>();
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        public T GetSingle(Guid guid)
        {
            return GetAll().FirstOrDefault(x => x.Guid == guid);
        }

        public virtual IQueryable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            return _entitiesContext.Set<T>().Where(predicate);
        }              

        public virtual void Add(T entity)
        {            
            entity.CreateDate = DateTime.Now;
            EntityEntry dbEntityEntry = _entitiesContext.Entry(entity);           
            _entitiesContext.Set<T>().Add(entity);
        }

        public virtual void Edit(T entity)
        {
            entity.ModifiedDate = DateTime.Now;            
            EntityEntry dbEntityEntry = _entitiesContext.Entry(entity);
            dbEntityEntry.Property("CreateDate").IsModified = false;
            dbEntityEntry.State = EntityState.Modified;
        }

        public virtual void Delete(T entity)
        {
            EntityEntry dbEntityEntry = _entitiesContext.Entry(entity);
            dbEntityEntry.State = EntityState.Deleted;
        }        

        public virtual bool Save()
        {
            try
            {
                _entitiesContext.SaveChanges();
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }
    }
}