﻿namespace Checkout.Domain.Migrations
{
    public interface IProductSeeder
    {
        void Seed();
        bool Empty();
    }
}