﻿using System;
using System.Linq;
using Checkout.Domain.Entities;

namespace Checkout.Domain.Migrations
{
    public class ProductSeeder : IProductSeeder
    {
        private readonly IEntityRepository<Product> _productRepository;
        public ProductSeeder(IEntityRepository<Product> productRepository)
        {
            _productRepository = productRepository;
        }

        public bool Empty()
        {
            return _productRepository.GetAll().Count() == 0;            
        }

        public void Seed()
        {
            if (!Empty())
            {
                return;
            }

            _productRepository.Add(new Product
            {
                CreateDate = DateTime.Now,
                ProductName = "Product 1",
                Price = 100,
                Guid = new Guid("2B8DD877-FDB8-4904-AD29-D734CE9A86AF")
            });

            _productRepository.Add(new Product
            {
                CreateDate = DateTime.Now,
                ProductName = "Product 2",
                Price = 110,
                Guid = new Guid("F35486A8-262C-4BD4-A2E2-D9EB052815CD")
            });

            _productRepository.Add(new Product
            {
                CreateDate = DateTime.Now,
                ProductName = "Product 3",
                Price = 150,
                Guid = new Guid("F973692E-0C9F-4014-969C-9B461FC0438D")
            });

            _productRepository.Save();
        }
    }
}
