﻿using System;
using System.Text;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Checkout.Clients
{
    internal static class ErrorFormatter
    {
        internal static IList<KeyValuePair<string, string>> KeyValueErrorFromJson(string jsonStr)
        {
            var errorContent = new List<KeyValuePair<string, string>>();
            using (var reader = new JsonTextReader(new System.IO.StringReader(jsonStr)))
            {
                string key = string.Empty;
                while (reader.Read())
                {
                    if (reader.TokenType == JsonToken.PropertyName)
                    {
                        if (reader.Value.ToString() == "key")
                        {
                            reader.Read();
                            key = reader.Value.ToString();
                        }
                        else if (reader.Value.ToString() == "errorMessage")
                        {
                            errorContent.Add(new KeyValuePair<string, string>(key, reader.Value.ToString()));
                        }
                    }
                }
            }
            return errorContent;
        }
    }
}
