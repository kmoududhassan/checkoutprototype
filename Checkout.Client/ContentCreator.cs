﻿using System.Text;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Checkout.Clients
{
    internal sealed class ByteContentCreator
    {
        private string _contentType;

        internal ByteContentCreator(string contentType = "application/json")
        {
            _contentType = contentType;
        }

        internal ByteArrayContent GetContent(object value)
        {
            string jsonRequest = JsonConvert.SerializeObject(value);

            var buffer = Encoding.UTF8.GetBytes(jsonRequest);
            var byteContent = new ByteArrayContent(buffer);

            byteContent.Headers.ContentType = new MediaTypeHeaderValue(_contentType);

            return byteContent;
        }
    }
}
