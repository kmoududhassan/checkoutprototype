﻿using System;
using System.Net.Http;
using System.Reflection;
using System.Diagnostics;
using System.Net.Http.Headers;

namespace Checkout.Clients
{
    internal sealed class HttpClientContext
    {
        private static HttpClientContext _clientContext;

        private HttpClient _httpClient;

        private HttpClientContext()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = new Uri("http://localhost:52928/");
            _httpClient.DefaultRequestHeaders.Accept.Clear();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            _httpClient.DefaultRequestHeaders.AcceptEncoding.Add(new StringWithQualityHeaderValue("gzip"));
            _httpClient.DefaultRequestHeaders.Add("X-UserAgent",
                string.Concat(assembly.FullName, "( ", FileVersionInfo.GetVersionInfo(assembly.Location).ProductVersion, ")"));
        }

        internal HttpClient HttpClient
        {
            get
            {
                return _httpClient;
            }
        }

        internal static HttpClientContext Context
        {
            get
            {
                if (null == _clientContext)
                {
                    _clientContext = new HttpClientContext();
                }
                return _clientContext;
            }
        }
    }
}
