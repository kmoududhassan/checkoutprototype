﻿using System;
using Newtonsoft.Json;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Runtime.Serialization.Json;

using Checkout.Model.RequestModels;
using Checkout.Model.DataTransferObjects;

namespace Checkout.Clients
{
    public class BasketClient : IBasketClient
    {
        private const string BasketItemBaseUri = "api/basketItem/";
        private const string BasketBaseUri = "api/basket/";
        private const string ProductBaseUri = "api/product/";

        IList<KeyValuePair<string, string>> ErrorContent;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="basketItemRequest"></param>
        /// <returns></returns>
        public async Task<BasketItemDto> AddToBasketAsync(BasketItemRequest basketItemRequest)
        {
            var contentCreator = new ByteContentCreator();
            var byteContent = contentCreator.GetContent(basketItemRequest);

            var httpResponseMessage = await HttpClientContext.Context.HttpClient.PostAsync(BasketItemBaseUri, byteContent);
            if (httpResponseMessage.IsSuccessStatusCode)
            {
                BasketItemDto basketItemDto = JsonConvert.DeserializeObject<BasketItemDto>(await httpResponseMessage.Content.ReadAsStringAsync());
                return basketItemDto;
            }

            SetErrorContent(httpResponseMessage);

            return null;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="basketGuid"></param>
        /// <returns></returns>
        public async Task<bool> ClearBasketAsync(Guid basketGuid)
        {
            var httpResponseMessage = await HttpClientContext.Context.HttpClient.DeleteAsync(BasketBaseUri + basketGuid);
            if (!httpResponseMessage.IsSuccessStatusCode)
            {
                SetErrorContent(httpResponseMessage);
            }
            return httpResponseMessage.IsSuccessStatusCode;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<BasketDto> CreateBasketAsync()
        {
            var serializer = new DataContractJsonSerializer(typeof(BasketDto));
            var streamTask = HttpClientContext.Context.HttpClient.GetStreamAsync(BasketBaseUri + "create");
            var basketDto = serializer.ReadObject(await streamTask) as BasketDto;
            return basketDto;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="basketItemGuid"></param>
        /// <returns></returns>
        public async Task<bool> DeleteFromBasketAsync(Guid basketItemGuid)
        {
            var httpResponseMessage = await HttpClientContext.Context.HttpClient.DeleteAsync(BasketItemBaseUri + basketItemGuid);
            if (!httpResponseMessage.IsSuccessStatusCode)
            {
                SetErrorContent(httpResponseMessage);
            }
            return httpResponseMessage.IsSuccessStatusCode;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="basketGuid"></param>
        /// <returns></returns>
        public async Task<BasketDto> GetBasketAsync(Guid basketGuid)
        {
            var serializer = new DataContractJsonSerializer(typeof(BasketDto));
            var streamTask = HttpClientContext.Context.HttpClient.GetStreamAsync(BasketBaseUri + basketGuid);
            var basketDto = serializer.ReadObject(await streamTask) as BasketDto;            
            return basketDto;                        
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="basketItemGuid"></param>
        /// <returns></returns>
        public async Task<BasketItemDto> GetBasketItemAsync(Guid basketItemGuid)
        {
            var serializer = new DataContractJsonSerializer(typeof(BasketItemDto));
            var streamTask = HttpClientContext.Context.HttpClient.GetStreamAsync(BasketItemBaseUri + basketItemGuid);
            var basketItemDto = serializer.ReadObject(await streamTask) as BasketItemDto;
            return basketItemDto;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<IList<ProductDto>> GetProductsAsync()
        {
            var serializer = new DataContractJsonSerializer(typeof(IList<ProductDto>));
            var streamTask = HttpClientContext.Context.HttpClient.GetStreamAsync(ProductBaseUri);
            var products = serializer.ReadObject(await streamTask) as IList<ProductDto>;
            return products;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="basketItemRequest"></param>
        /// <returns></returns>
        public async Task<BasketItemDto> UpdateBasketAsync(BasketItemRequest basketItemRequest)
        {
            var contentCreator = new ByteContentCreator();
            var byteContent = contentCreator.GetContent(basketItemRequest);

            var httpResponseMessage = await HttpClientContext.Context.HttpClient.PutAsync(BasketItemBaseUri, byteContent);

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                return await GetBasketItemAsync(basketItemRequest.BasketItemGuid);
            }
            return null;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="basketGuid"></param>
        public async Task ArchiveBasketAsync(Guid basketGuid)
        {            
            var request = new BasketRequest{ BasketGuid = basketGuid, IsActive = false };
            var contentCreator = new ByteContentCreator();
            var byteContent = contentCreator.GetContent(request);
            await HttpClientContext.Context.HttpClient.PutAsync(BasketBaseUri, byteContent);
        }
        private async void SetErrorContent(HttpResponseMessage httpResponseMessage)
        {
            string jsonStr = await httpResponseMessage.Content.ReadAsStringAsync();

            ErrorContent = ErrorFormatter.KeyValueErrorFromJson(jsonStr);
        }
    }
}
