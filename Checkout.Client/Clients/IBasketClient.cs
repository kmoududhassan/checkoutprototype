﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

using Checkout.Model.RequestModels;
using Checkout.Model.DataTransferObjects;

namespace Checkout.Clients
{
    public interface IBasketClient
    {
        Task<BasketItemDto> AddToBasketAsync(BasketItemRequest basketItemRequest);
        Task<BasketItemDto> GetBasketItemAsync(Guid basketItemGuid);
        Task<BasketItemDto> UpdateBasketAsync(BasketItemRequest basketItemRequest);
        Task<bool> DeleteFromBasketAsync(Guid basketItemGuid);
        Task<bool> ClearBasketAsync(Guid basketGuid);
        Task<BasketDto> GetBasketAsync(Guid basketGuid);
        Task<BasketDto> CreateBasketAsync();
        Task ArchiveBasketAsync(Guid basketGuid);
        Task<IList<ProductDto>> GetProductsAsync();
    }
}
