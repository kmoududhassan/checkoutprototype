using Moq;
using Xunit;
using System;
using Microsoft.AspNetCore.Mvc;

using Checkout.Domain.Entities;
using Checkout.API.Controllers;
using Checkout.Domain.Services;
using Checkout.Model.RequestModels;

namespace Checkout.Test
{
    public class BasketItemControllerTest
    {
        [Fact]
        public void Post_Should_Return_NotFound_When_Product_Does_Not_Exist()
        {
            //arrange
            var mockBasketService = new Mock<IBasketService>();
            mockBasketService.Setup(x => x.IsProductExist(It.IsAny<Guid>()))
                .Returns(false);
            var controller = new BasketItemController(mockBasketService.Object);

            //act
            IActionResult result = controller.Post(new BasketItemRequest());

            //assert
            Assert.IsType<NotFoundObjectResult>(result);
        }

        [Fact]
        public void Post_Should_Return_NotFound_When_Basket_Does_Not_Exist()
        {
            //arrange
            var mockBasketService = new Mock<IBasketService>();
            mockBasketService.Setup(x => x.IsBasketExist(It.IsAny<Guid>()))
                .Returns(false);
            var controller = new BasketItemController(mockBasketService.Object);

            //act
            IActionResult result = controller.Post(new BasketItemRequest());

            //assert
            Assert.IsType<NotFoundObjectResult>(result);
        }

        [Fact]
        public void Post_Should_Return_Success_When_Product_Basket_Exist_And_Item_Added()
        {
            //arrange
            var mockBasketService = new Mock<IBasketService>();

            mockBasketService.Setup(x => x.IsProductExist(It.IsAny<Guid>()))
                .Returns(true);

            mockBasketService.Setup(x => x.IsBasketExist(It.IsAny<Guid>()))
                .Returns(true);

            mockBasketService.Setup(x => x.AddItemToBasket(It.IsAny<BasketItem>()))
                .Returns(new Result<BasketItem>(true)
                {
                    Entity = new BasketItem()                    
                });

            var controller = new BasketItemController(mockBasketService.Object);

            //act
            IActionResult result = controller.Post(new BasketItemRequest());

            //assert
            Assert.IsType<CreatedAtRouteResult>(result);
        }
    }
}
