﻿using Checkout.Domain.Entities;
using Checkout.Model.DataTransferObjects;

namespace Checkout.API.Mappers
{
    internal static class ProductExtensions
    {
        internal static ProductDto ToProductDto(this Product product)
        {
            return new ProductDto
            {                
                Guid = product.Guid,
                IsAvailable = product.ProductStatus == ProductStatus.Available,
                Price = product.Price,
                ProductName = product.ProductName
            };
        }
    }
}