﻿using Checkout.Domain.Entities;
using Checkout.Model.DataTransferObjects;

namespace Checkout.API.Mappers
{
    internal static class BasketItemExtensions
    {
        internal static BasketItemDto ToBasketItemDto(this BasketItem basketItem)
        {
            return new BasketItemDto
            {
                BasketItemGuid = basketItem.Guid,
                ProductGuid = basketItem.ProductGuid,
                BasketGuid = basketItem.BasketGuid,
                ProductName = basketItem.Product != null ? basketItem.Product.ProductName : string.Empty,
                Quantity = basketItem.Quantity
            };
        }
    }
}