﻿using Checkout.Model.RequestModels;
using Checkout.Domain.Entities;

namespace Checkout.API.Mappers
{
    internal static class BasketItemRequestExtension
    {
        internal static BasketItem ToBasketItem(this BasketItemRequest requestModel)
        {
            return new BasketItem
            {
                BasketGuid = requestModel.BasketGuid,
                Guid = requestModel.BasketItemGuid,
                ProductGuid = requestModel.ProductGuid,
                Quantity = requestModel.Quantity
            };
        }
    }
}
