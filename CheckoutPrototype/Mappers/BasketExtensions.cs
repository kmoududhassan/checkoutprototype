﻿using System.Linq;

using Checkout.Domain.Entities;
using Checkout.Model.DataTransferObjects;

namespace Checkout.API.Mappers
{
    internal static class BasketExtensions
    {
        internal static BasketDto ToBasketDto(this Basket basket)
        {
            return new BasketDto
            {                
                BasketGuid = basket.Guid,
                IsActive = basket.IsActive,
                TotalItems = basket.BasketItems.Count,
                UserGuid = basket.UserGuid,
                BasketItems = basket.BasketItems.Select(x => x.ToBasketItemDto())
                                .ToList()
            };
        }
    }
}