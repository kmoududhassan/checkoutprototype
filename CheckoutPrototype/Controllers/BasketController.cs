﻿using System;
using System.Net;
using Microsoft.AspNetCore.Mvc;

using Checkout.API.Mappers;
using Checkout.API.Filters;
using Checkout.Domain.Services;
using Checkout.Model.RequestModels;

namespace Checkout.API.Controllers
{
    [Route("api/[controller]")]
    public class BasketController : Controller
    {
        private readonly IBasketService _basketService;    
        
        public BasketController(IBasketService basketService)
        {
            _basketService = basketService;            
        }

        [HttpGet("{guid}")]
        public IActionResult Get(Guid guid)
        {
            var basket = _basketService.GetBasket(guid);

            if (null == basket)
            {
                return NotFound();
            }
            return new ObjectResult(basket.ToBasketDto());
        }

        [HttpGet("Create")]
        public IActionResult Create()
        {
            var basket = _basketService.CreateEmptyBasket();
            return Ok(basket.ToBasketDto());
        }

        [HttpPut]
        [ValidateModel]
        public IActionResult Archive([FromBody] BasketRequest basketRequest)
        {
            _basketService.ArchiveBasket(basketRequest.BasketGuid);
            return Ok();
        }

        [HttpDelete("{guid}")]
        public IActionResult Clear(Guid guid)
        {
            var basket = _basketService.GetBasket(guid);

            if (null == basket)
            {
                return NotFound();
            }

            var result = _basketService.ClearBasket(guid);

            if (result.Success)
            {
                return NoContent();
            }
            return StatusCode((int)HttpStatusCode.InternalServerError);
        }
    }
}
