﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;

using Checkout.API.Mappers;
using Checkout.Domain.Services;

namespace Checkout.API.Controllers
{
    [Route("api/[controller]")]
    public class ProductController : Controller
    {
        private readonly IShopItemService _shopItemService;        

        public ProductController(IShopItemService shopItemService)
        {
            _shopItemService = shopItemService;            
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var products = _shopItemService.GetProducts()
                                .Select(x => x.ToProductDto())
                                .ToList();
            return new ObjectResult(products);
        }
    }
}
