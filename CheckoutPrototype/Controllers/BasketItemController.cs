﻿using System;
using System.Net;
using Microsoft.AspNetCore.Mvc;

using Checkout.API.Mappers;
using Checkout.API.Filters;
using Checkout.API.ErrorMessages;
using Checkout.Domain.Services;
using Checkout.Model.RequestModels;

namespace Checkout.API.Controllers
{
    [Route("api/[controller]")]
    public class BasketItemController : Controller
    {
        private readonly IBasketService _basketService;        

        public BasketItemController(IBasketService basketService)
        {
            _basketService = basketService;            
        }

        [HttpGet("{guid}", Name = "GetBasketItem")]
        public IActionResult Get(Guid guid)
        {
            var item = _basketService.GetBasketItem(guid);
            if (null == item)
            {
                return NotFound();
            }
            return new ObjectResult(item.ToBasketItemDto());
        }

        [HttpPost]
        [ValidateModel]
        public IActionResult Post([FromBody]BasketItemRequest basketItemRequest)
        {
            bool isValid = true;
            if (!_basketService.IsProductExist(basketItemRequest.ProductGuid))
            {
                ModelState.AddModelError("ProductGuid", ValidationErrorKeyMessages.ErrorMessages["ProductGuid"][ErrorType.NotExist]);
                isValid = false;                
            }

            if (basketItemRequest.BasketGuid != Guid.Empty
                && !_basketService.IsBasketExist(basketItemRequest.BasketGuid))
            {
                ModelState.AddModelError("BasketGuid", ValidationErrorKeyMessages.ErrorMessages["BasketGuid"][ErrorType.NotExist]);
                isValid = false;                
            }

            if (!isValid)
                return NotFound(ModelState);
                        
            var result = _basketService.AddItemToBasket(basketItemRequest.ToBasketItem());

            if (result.Success)
            {                
                return CreatedAtRoute("GetBasketItem", new { guid = result.Entity.Guid }, result.Entity.ToBasketItemDto());
            }

            return StatusCode((int)HttpStatusCode.InternalServerError);
        }

        [HttpPut]
        [ValidateModel]
        public IActionResult Put([FromBody] BasketItemRequest basketItemRequest)
        {
            if (!_basketService.IsBasketItemExist(basketItemRequest.BasketItemGuid))
            {
                ModelState.AddModelError("BasketItemGuid", ValidationErrorKeyMessages.ErrorMessages["BasketItemGuid"][ErrorType.NotExist]);

                return NotFound(ModelState);
            }

            var result = _basketService.UpdateBasketItem(basketItemRequest.ToBasketItem());

            if (result.Success)
            {
                return NoContent();
            }
            return StatusCode((int)HttpStatusCode.InternalServerError);
        }

        [HttpDelete("{guid}")]
        public IActionResult Delete(Guid guid)
        {
            if (!_basketService.IsBasketItemExist(guid))
            {
                NotFound();
            }

            var result = _basketService.DeleteBasketItem(guid);

            if (result.Success)
            {
                return NoContent();
            }
            return StatusCode((int)HttpStatusCode.InternalServerError);
        }
    }
}
