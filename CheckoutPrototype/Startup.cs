﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using Checkout.Domain.Entities;
using Checkout.Domain.Services;
using Checkout.Domain.Migrations;

namespace Checkout.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<DbContext, EntitiesContext>();
            services.AddDbContext<EntitiesContext>(opt => opt.UseInMemoryDatabase("CheckoutPrototype"));            
            services.AddTransient<IEntityRepository<BasketItem>, EntityRepository<BasketItem>>();
            services.AddTransient<IEntityRepository<Basket>, EntityRepository<Basket>>();
            services.AddTransient<IEntityRepository<Product>, EntityRepository<Product>>();
            services.AddTransient<IBasketService, BasketService>();
            services.AddTransient<IProductSeeder, ProductSeeder>();
            services.AddTransient<IShopItemService, ShopItemService>();
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IProductSeeder seeder)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                seeder.Seed();
            }

            app.UseMvc();
        }
    }
}
