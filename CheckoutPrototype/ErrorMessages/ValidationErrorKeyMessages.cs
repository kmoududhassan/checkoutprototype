﻿using System.Collections.Generic;

namespace Checkout.API.ErrorMessages
{
    internal enum ErrorType
    {
        Empty,
        NotExist,
        Type1,
        Type2
    }

    internal class ValidationErrorKeyMessages
    {
        public static IDictionary<string, IDictionary<ErrorType, string>> ErrorMessages = new Dictionary<string, IDictionary<ErrorType, string>>
        {
            {
                "BasketGuid",
                new Dictionary<ErrorType, string>
                {
                    { ErrorType.NotExist, "No basket associated with this guid" }
                }
            },
            {
                "Quantity",
                new Dictionary<ErrorType, string>
                {
                    { ErrorType.Type1, "Quantity must be greater than 0" }
                }
            },
            {
                "ProductGuid",
                new Dictionary<ErrorType, string>
                {
                    { ErrorType.NotExist, "Product does not exist!" }
                }
            },
            {
                "BasketItemGuid",
                new Dictionary<ErrorType, string>
                {
                    { ErrorType.NotExist, "No item found!" }
                }
            },
        };
    }
}
