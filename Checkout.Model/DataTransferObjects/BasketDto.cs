﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace Checkout.Model.DataTransferObjects
{
    [DataContract(Name = "BasketDto")]
    public class BasketDto
    {
        [Key]
        [DataMember(Name = "BasketGuid")]
        public Guid BasketGuid { get; set; }

        [DataMember(Name = "IsActive")]
        public bool IsActive { get; set; }

        [DataMember(Name = "TotalItems")]
        public int TotalItems { get; set; }        

        [DataMember(Name = "UserGuid")]
        public Guid? UserGuid { get; set; }

        [DataMember(Name = "BasketItems")]
        public IList<BasketItemDto> BasketItems { get; set; }        
    }
}
