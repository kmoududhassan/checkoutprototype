﻿using System;
using System.Runtime.Serialization;

namespace Checkout.Model.DataTransferObjects
{
    [DataContract(Name = "BasketItemDto")]
    public class BasketItemDto
    {
        [DataMember(Name = "BasketItemGuid")]
        public Guid BasketItemGuid { get; set; }

        [DataMember(Name = "ProductGuid")]
        public Guid ProductGuid { get; set; }

        [DataMember(Name = "BasketGuid")]
        public Guid BasketGuid { get; set; }

        [DataMember(Name = "Quantity")]
        public int Quantity { get; set; }

        [DataMember(Name = "ProductName")]
        public string ProductName { get; set; }        
    }
}
