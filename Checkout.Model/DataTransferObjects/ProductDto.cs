﻿using System;
using System.Runtime.Serialization;

namespace Checkout.Model.DataTransferObjects
{
    [DataContract(Name = "ProductDto")]
    public class ProductDto
    {        
        [DataMember(Name = "Guid")]
        public Guid Guid { get; set; }

        [DataMember(Name = "Price")]
        public decimal Price { get; set; }

        [DataMember(Name = "IsAvailable")]
        public bool IsAvailable { get; set; }

        [DataMember(Name = "ProductName")]
        public string ProductName { get; set; }        
    }
}
