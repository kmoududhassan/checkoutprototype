﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Checkout.Model.RequestModels
{
    public class BasketRequest
    {        
        [Required]
        public Guid BasketGuid { get; set; }
        
        public Guid? UserGuid { get; set; }

        public bool IsActive { get; set; }
    }
}
