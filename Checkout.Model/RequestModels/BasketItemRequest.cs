﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Checkout.Model.RequestModels
{
    public class BasketItemRequest
    {        
        public Guid BasketGuid { get; set; }

        public Guid BasketItemGuid { get; set; }

        [Required]
        public Guid ProductGuid { get; set; }

        [Required]
        public int Quantity { get; set; }        
    }
}
